FROM docker.repos.balad.ir/python:3.8

WORKDIR /usr/src/apps

COPY ./requirements.txt .
RUN pip install -r ./requirements.txt
EXPOSE 8000
COPY . .
CMD ["/bin/bash", "entry_point.sh"]