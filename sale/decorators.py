from http import HTTPStatus
from functools import wraps
import typing


from django.http import JsonResponse

from sale.static_messages import MESSAGE, LOGIN_REQUIRED


class LoginRequired:
    def __init__(self, func: typing.Callable):
        self.func = func

    def __call__(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return JsonResponse(
                {MESSAGE: LOGIN_REQUIRED},
                status=HTTPStatus.UNAUTHORIZED,
            )
        return self.func(request, *args, **kwargs)


def login_required(func):
    @wraps(func)
    def wrapper(request, *args, **kwargs):
        if request.user.is_authenticated:
            return func(request, *args, **kwargs)
        else:
            return JsonResponse(
                {MESSAGE: LOGIN_REQUIRED},
                status=HTTPStatus.UNAUTHORIZED,
            )

    return wrapper

