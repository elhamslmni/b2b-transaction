from django.core.exceptions import ValidationError
from django.db import models
from django.contrib.auth.models import AbstractUser
from django.core.validators import MinValueValidator
from sale.static_messages import CREDIT_AMOUNT_SHOULD_BE_POSITIVE


class Seller(AbstractUser):
    credit = models.IntegerField(default=0, validators=[MinValueValidator(0)])

    def save(self, *args, **kwargs):
        if self.credit < 0:
            raise ValidationError(CREDIT_AMOUNT_SHOULD_BE_POSITIVE)
        super().save(*args, **kwargs)

    def update_credit(self, dela_credit: int):
        self.credit = self.credit + dela_credit


class Transaction(models.Model):
    seller = models.ForeignKey(
        "sale.Seller",
        on_delete=models.DO_NOTHING,
        related_name="records",
    )
    amount = models.IntegerField(null=False)
    is_valid = models.BooleanField(default=False)
    creation_time = models.DateTimeField(auto_now_add=True)
