import json
from http import HTTPStatus

from django.test import TestCase, Client
from django.urls import reverse

from sale.static_messages import (
    DUPLICATE_USERNAME,
    ONE_OF_REQUIRED_FIELD_IS_NULL,
    USERNAME_OR_PASSWORD_IS_NULL,
    USER_NOT_FOUND,
    MESSAGE,
)


class TestSignIn(TestCase):
    def test_register_success(self):
        client = Client()
        response = client.put(
            reverse("sale:register"),
            json.dumps({"username": "aaa", "password": "1122"}),
            content_type="application/json",
        )
        self.assertEqual(
            response.status_code,
            HTTPStatus.CREATED,
        )

    def test_register_with_missing_args_error(self):
        client = Client()
        response = client.put(
            reverse("sale:register"),
            json.dumps({"username": "aaa"}),
            content_type="application/json",
        )
        self.assertEqual(
            response.json()[MESSAGE],
            ONE_OF_REQUIRED_FIELD_IS_NULL,
        )

    def test_register_with_duplicate_username_error(self):
        client = Client()
        client.put(
            reverse("sale:register"),
            json.dumps({"username": "aaa", "password": "1122"}),
            content_type="application/json",
        )
        response = client.put(
            reverse("sale:register"),
            json.dumps({"username": "aaa", "password": "1122"}),
            content_type="application/json",
        )
        self.assertEqual(
            response.json()[MESSAGE],
            DUPLICATE_USERNAME,
        )

    def test_login_without_register_not_found_error(self):
        client = Client()
        response = client.put(
            reverse("sale:login"),
            json.dumps({"username": "aaa", "password": "1122"}),
            content_type="application/json",
        )
        self.assertEqual(
            response.json()[MESSAGE],
            USER_NOT_FOUND,
        )
