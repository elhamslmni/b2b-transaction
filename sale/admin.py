from django.contrib import admin

from sale.models import Seller


admin.site.register(Seller)
