from django.urls import path

from .views import IncreaseCredit, BuySimCardCredit, login, register, ShowTransactionsData

app_name = "sale"

urlpatterns = [
    path("buy-simcard-credit/", BuySimCardCredit.as_view(), name="buy-simcard-credit"),
    path("increase-credit/", IncreaseCredit.as_view(), name="increase-credit"),
    path("login/", login, name="login"),
    path("register/", register, name="register"),
    path("show-transactions-data/", ShowTransactionsData.as_view(), name="transactions-data"),
]
