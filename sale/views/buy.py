import json
from json import JSONDecodeError
from http import HTTPStatus
import logging

from django.db import transaction
from django.http import JsonResponse
from django.utils.decorators import method_decorator
from django.views import View
from django.views.decorators.csrf import csrf_exempt

from sale.decorators import LoginRequired
from sale.models import Transaction, Seller
from sale.static_messages import (
    MESSAGE,
    CREDIT_AMOUNT_SHOULD_BE_POSITIVE,
)


@method_decorator(csrf_exempt, name="dispatch")
@method_decorator(LoginRequired, name="post")
class BuySimCardCredit(View):
    def post(self, request):
        try:
            data = json.loads(request.body)
        except JSONDecodeError:
            return JsonResponse({}, status=HTTPStatus.BAD_REQUEST)
        print(f"new request {data.get('pid')}")

        if "credit" not in data:
            return JsonResponse({}, status=HTTPStatus.BAD_REQUEST)

        credit = int(data["credit"])
        seller = request.user
        logging.warn(f"{seller.username} requested for buy credit")

        if credit < 0:
            return JsonResponse(
                {MESSAGE: CREDIT_AMOUNT_SHOULD_BE_POSITIVE},
                status=HTTPStatus.BAD_REQUEST,
            )

        with transaction.atomic():
            seller = Seller.objects.select_for_update().get(id=seller.id)
            seller.update_credit(dela_credit=-1 * credit)
            seller.save()
            Transaction.objects.create(
                seller=seller,
                amount=-1 * credit,
                is_valid=True,
            )

        return JsonResponse(
            {},
            status=HTTPStatus.OK,
        )
