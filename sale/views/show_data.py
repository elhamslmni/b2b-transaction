from http import HTTPStatus

from django.http import JsonResponse
from django.utils.decorators import method_decorator
from django.views import View
from django.views.decorators.csrf import csrf_exempt

from sale.decorators import LoginRequired
from sale.models import Transaction


@method_decorator(csrf_exempt, name="dispatch")
@method_decorator(LoginRequired, name="get")
class ShowTransactionsData(View):
    def get(self, request):
        transactions_data = [
            {"time": t.creation_time, "amount": t.amount}
            for t in Transaction.objects.filter(
                is_valid=True, seller_id=request.user.id
            )
        ]

        return JsonResponse(
            {
                "transactions": transactions_data,
                "seller_credit": request.user.credit,
            },
            status=HTTPStatus.OK,
        )
