from http import HTTPStatus

from django.contrib.auth import logout as django_logout
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_http_methods

from sale.decorators import login_required


@csrf_exempt
@require_http_methods(["POST"])
@login_required
def logout(request):
    django_logout(request)
    return JsonResponse({}, status=HTTPStatus.OK)
