import json
from json import JSONDecodeError
from http import HTTPStatus
import jwt
from django.contrib.auth import authenticate

from django.core.exceptions import ValidationError
from django.db import IntegrityError
from django.http import JsonResponse
from django.utils import timezone
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_http_methods
from django.conf import settings

from sale.models import Seller
from sale.static_messages import (
    DUPLICATE_USERNAME,
    ONE_OF_REQUIRED_FIELD_IS_NULL,
    USER_NOT_FOUND,
    MESSAGE,
)


@csrf_exempt
@require_http_methods(["POST"])
def login(request):
    try:
        data = json.loads(request.body)
    except JSONDecodeError:
        return JsonResponse({}, status=HTTPStatus.BAD_REQUEST)

    if "username" not in data or "password" not in data:
        return JsonResponse(
            {MESSAGE: ONE_OF_REQUIRED_FIELD_IS_NULL},
            status=HTTPStatus.BAD_REQUEST,
        )

    seller = authenticate(username=data["username"], password=data["password"])
    if seller is None:
        return JsonResponse(
            {MESSAGE: USER_NOT_FOUND},
            status=HTTPStatus.NOT_FOUND,
        )
    return get_login_success_response(seller)


def get_login_success_response(user):
    token = jwt.encode(
        {"username": user.username, "exp": timezone.now() + settings.LOGIN_VALID_TIME},
        settings.SECRET_KEY,
    )

    return JsonResponse({"token": token}, status=HTTPStatus.OK)


@csrf_exempt
@require_http_methods(["PUT"])
def register(request):
    try:
        data = json.loads(request.body)
    except JSONDecodeError:
        return JsonResponse({}, status=HTTPStatus.BAD_REQUEST)

    if "username" not in data or "password" not in data:
        return JsonResponse(
            {MESSAGE: ONE_OF_REQUIRED_FIELD_IS_NULL},
            status=HTTPStatus.NOT_ACCEPTABLE,
        )

    try:
        s = Seller(username=data["username"])
        s.set_password(data["password"])
        s.clean_fields()
        s.save()
    except ValidationError as e:
        return JsonResponse({}, status=HTTPStatus.BAD_REQUEST)
    except IntegrityError as e:
        return JsonResponse(
            {
                MESSAGE: DUPLICATE_USERNAME,
            },
            status=HTTPStatus.CONFLICT,
        )

    return JsonResponse({}, status=HTTPStatus.CREATED)
