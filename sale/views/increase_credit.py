from http import HTTPStatus
import json
from json import JSONDecodeError
import logging

from django.db import transaction
from django.http import JsonResponse
from django.utils.decorators import method_decorator
from django.views import View
from django.views.decorators.csrf import csrf_exempt

from sale.decorators import LoginRequired
from sale.models import Transaction, Seller
from sale.static_messages import (
    MESSAGE,
    CREDIT_AMOUNT_SHOULD_BE_POSITIVE,
    PLEASE_ENTER_CREDIT_CORRECTLY,
)

logger = logging.getLogger("logger")


@method_decorator(csrf_exempt, name="dispatch")
@method_decorator(LoginRequired, name="post")
class IncreaseCredit(View):
    def post(self, request):
        try:
            data = json.loads(request.body)
        except JSONDecodeError:
            return JsonResponse({}, status=HTTPStatus.BAD_REQUEST)

        try:
            credit = int(data["credit"])
        except:
            return JsonResponse(
                {MESSAGE: PLEASE_ENTER_CREDIT_CORRECTLY},
                status=HTTPStatus.BAD_REQUEST,
            )

        seller = request.user
        logger.warn(f"{seller.username} requested for increase credit")
        if credit < 0:
            return JsonResponse(
                {MESSAGE: CREDIT_AMOUNT_SHOULD_BE_POSITIVE},
                status=HTTPStatus.BAD_REQUEST,
            )

        with transaction.atomic():
            seller = Seller.objects.select_for_update().get(id=seller.id)
            seller.update_credit(dela_credit=credit)
            seller.save()
            Transaction.objects.create(
                seller=seller,
                amount=credit,
                is_valid=True,
            )
        return JsonResponse({}, status=HTTPStatus.OK)
