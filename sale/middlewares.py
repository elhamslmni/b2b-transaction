import jwt
from django.conf import settings
from django.contrib.auth.models import AnonymousUser
from django.utils.functional import SimpleLazyObject

from sale.models import Seller


class JWTMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        if not request.path.startswith("/admin"):
            request.user = SimpleLazyObject(lambda: self.get_user(request))
        return self.get_response(request)

    @staticmethod
    def get_user(request):
        token = request.headers.get("x-access-token")
        if token is None:
            return AnonymousUser()

        try:
            data = jwt.decode(token, settings.SECRET_KEY, algorithms="HS256")
        except:
            return AnonymousUser()

        try:
            return Seller.objects.get(username=data["username"])
        except:
            return AnonymousUser()
