import time
from multiprocessing import Process
import requests


def buy_simcard_credit(token, amount):
    cookies = {
        "csrftoken": "ZfYC7CFf1jOlCGvSnsuCjr0Uqabtf8ajNviYqk8l2dZbsakzU8LwdAwpScpoqsRv",
        "sessionid": "dvoy0arwy8vjmajxpqf34jqlu9xu3555",
    }

    headers = {
        "x-access-token": token,
        "Content-Type": "application/json",
    }

    json_data = {
        "credit": amount,
    }

    requests.post(
        "http://127.0.0.1:8000/api/buy-simcard-credit/",
        cookies=cookies,
        headers=headers,
        json=json_data,
    )


def increase_credit(token, amount):
    cookies = {
        "csrftoken": "ZfYC7CFf1jOlCGvSnsuCjr0Uqabtf8ajNviYqk8l2dZbsakzU8LwdAwpScpoqsRv",
        "sessionid": "dvoy0arwy8vjmajxpqf34jqlu9xu3555",
    }

    headers = {
        "x-access-token": token,
        "Content-Type": "application/json",
    }

    json_data = {
        "credit": amount,
    }

    requests.post(
        "http://127.0.0.1:8000/api/increase-credit/",
        cookies=cookies,
        headers=headers,
        json=json_data,
    )


def create_or_get_tokens(username, password):
    cookies = {
        "csrftoken": "ZfYC7CFf1jOlCGvSnsuCjr0Uqabtf8ajNviYqk8l2dZbsakzU8LwdAwpScpoqsRv",
        "sessionid": "dvoy0arwy8vjmajxpqf34jqlu9xu3555",
    }

    headers = {
        "Content-Type": "application/json",
    }

    json_data = {
        "username": username,
        "password": password,
    }

    requests.put(
        "http://127.0.0.1:8000/api/register/",
        cookies=cookies,
        headers=headers,
        json=json_data,
    )
    response = requests.post(
        "http://127.0.0.1:8000/api/login/",
        cookies=cookies,
        headers=headers,
        json=json_data,
    )
    return response.json()["token"]


def get_user_transactions_data(token):
    cookies = {
        "csrftoken": "ZfYC7CFf1jOlCGvSnsuCjr0Uqabtf8ajNviYqk8l2dZbsakzU8LwdAwpScpoqsRv",
        "sessionid": "dvoy0arwy8vjmajxpqf34jqlu9xu3555",
    }

    headers = {
        "x-access-token": token,
        "Content-Type": "application/json",
    }

    json_data = {}

    response = requests.get(
        "http://127.0.0.1:8000/api/show-transactions-data/",
        cookies=cookies,
        headers=headers,
        json=json_data,
    )
    return response


if __name__ == "__main__":

    token1 = create_or_get_tokens(username="test11111", password="1122")
    token2 = create_or_get_tokens(username="test21111", password="1122")

    user_1_credit = get_user_transactions_data(token1).json()["seller_credit"]
    user_2_credit = get_user_transactions_data(token2).json()["seller_credit"]

    increase_credit(token1, 3000)
    increase_credit(token2, 3000)

    processes = []
    for _ in range(10):
        processes.append(
            Process(
                target=increase_credit,
                args=(token1, 100),
            )
        )
        processes.append(
            Process(
                target=buy_simcard_credit,
                args=(token1, 50),
            )
        )
        processes.append(
            Process(
                target=increase_credit,
                args=(token2, 100),
            )
        )
        processes.append(
            Process(
                target=buy_simcard_credit,
                args=(token2, 50),
            )
        )

    for p in processes:
        p.start()
    for p in processes:
        p.join()

    user1_data = get_user_transactions_data(token1).json()
    user2_data = get_user_transactions_data(token1).json()

    user_1_new_credit = user1_data["seller_credit"]
    user_2_new_credit = user2_data["seller_credit"]

    if user_1_new_credit - user_1_credit != 3500:
        exit(1)

    if user_2_new_credit - user_2_credit != 3500:
        exit(1)

    user1_transactions = user1_data["transactions"]
    user2_transactions = user2_data["transactions"]

    print([t['amount'] for t in user1_transactions])
    print([t['amount'] for t in user2_transactions])

    sum1 = 0
    sum2 = 0

    for t in user1_transactions:
        sum1 += t["amount"]

    for t in user2_transactions:
        sum2 += t["amount"]

    if sum1 != user_1_new_credit:
        print(sum1, user_1_new_credit)
        exit(1)

    if sum2 != user_2_new_credit:
        print(sum2, user_2_new_credit)
        exit(1)
