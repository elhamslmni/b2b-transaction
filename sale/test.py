import json
import threading
import time
from http import HTTPStatus
from multiprocessing import Process

import requests
from django.db import connection
from django.test import TestCase, Client, TransactionTestCase
from django.urls import reverse

from sale.models import Seller, Transaction
from sale.static_messages import (
    DUPLICATE_USERNAME,
    ONE_OF_REQUIRED_FIELD_IS_NULL,
    USER_NOT_FOUND,
    MESSAGE,
    CREDIT_AMOUNT_SHOULD_BE_POSITIVE,
)


class TestSignIn(TestCase):
    def test_register_success(self):
        client = Client()
        response = client.put(
            reverse("sale:register"),
            json.dumps({"username": "aaa", "password": "1122"}),
            content_type="application/json",
        )
        self.assertEqual(
            response.status_code,
            HTTPStatus.CREATED,
        )

    def test_register_with_missing_args_error(self):
        client = Client()
        response = client.put(
            reverse("sale:register"),
            json.dumps({"username": "aaa"}),
            content_type="application/json",
        )
        self.assertEqual(
            response.json()[MESSAGE],
            ONE_OF_REQUIRED_FIELD_IS_NULL,
        )

    def test_register_with_duplicate_username_error(self):
        client = Client()
        client.put(
            reverse("sale:register"),
            json.dumps({"username": "aaa", "password": "1122"}),
            content_type="application/json",
        )
        response = client.put(
            reverse("sale:register"),
            json.dumps({"username": "aaa", "password": "1122"}),
            content_type="application/json",
        )
        self.assertEqual(
            response.json()[MESSAGE],
            DUPLICATE_USERNAME,
        )

    def test_login_without_register_not_found_error(self):
        client = Client()
        response = client.post(
            reverse("sale:login"),
            json.dumps({"username": "aaa", "password": "1122"}),
            content_type="application/json",
        )
        self.assertEqual(
            response.json()[MESSAGE],
            USER_NOT_FOUND,
        )

    def test_login_with_missing_args_error(self):
        client = Client()
        response = client.post(
            reverse("sale:login"),
            json.dumps({"username": "aaa"}),
            content_type="application/json",
        )
        self.assertEqual(
            response.json()[MESSAGE],
            ONE_OF_REQUIRED_FIELD_IS_NULL,
        )

    def test_login_success(self):
        client = Client()
        client.put(
            reverse("sale:register"),
            json.dumps({"username": "aaa", "password": "1122"}),
            content_type="application/json",
        )
        response = client.post(
            reverse("sale:login"),
            json.dumps({"username": "aaa", "password": "1122"}),
            content_type="application/json",
        )
        self.assertEqual(
            response.status_code,
            HTTPStatus.OK,
        )


class TestTransaction(TestCase):
    def setUp(self) -> None:
        self.user = Seller(username="test")
        self.user.set_password("test")
        self.user.save()

    def login_and_get_token(self, username: str, password: str) -> str:
        client = Client()
        body = {"username": username, "password": password}
        response = client.post(
            reverse("sale:login"),
            json.dumps(body),
            content_type="application/json",
        )
        return response.json()["token"]

    def buy_sim_card_credit(self, credit, token=""):
        client = Client()
        body = {"credit": credit}
        response = client.post(
            reverse("sale:buy-simcard-credit"),
            json.dumps(body),
            content_type="application/json",
            HTTP_X_ACCESS_TOKEN=token,
        )
        return response

    def increase_credit(self, credit, token=""):
        client = Client()
        body = {"credit": credit}
        response = client.post(
            reverse("sale:increase-credit"),
            json.dumps(body),
            content_type="application/json",
            HTTP_X_ACCESS_TOKEN=token,
        )
        return response

    def test_increase_credit_unauthorized_user_error(self):
        response = self.increase_credit(credit=100, token="kkk")
        self.assertEqual(
            response.status_code,
            HTTPStatus.UNAUTHORIZED,
        )

    def test_increase_credit_authorized_user_negative_credit_error(self):
        token = self.login_and_get_token(username="test", password="test")
        response = self.increase_credit(credit=-100, token=token)
        self.assertEqual(
            response.json()[MESSAGE],
            CREDIT_AMOUNT_SHOULD_BE_POSITIVE,
        )

    def test_increase_credit_authorized_user_positive_credit_success(self):
        token = self.login_and_get_token(username="test", password="test")
        response = self.increase_credit(credit=100, token=token)
        self.assertEqual(
            response.status_code,
            HTTPStatus.OK,
        )

    def test_by_simcard_credit_unauthorized_user_error(self):
        response = self.buy_sim_card_credit(credit=100, token="")
        self.assertEqual(
            response.status_code,
            HTTPStatus.UNAUTHORIZED,
        )

    def test_by_simcard_credit_authorized_user_negative_credit_error(self):
        token = self.login_and_get_token(username="test", password="test")
        response = self.buy_sim_card_credit(credit=-100, token=token)
        self.assertEqual(
            response.json()[MESSAGE],
            CREDIT_AMOUNT_SHOULD_BE_POSITIVE,
        )

    def test_by_simcard_credit_authorized_user_invalid_credit_error(self):
        token = self.login_and_get_token(username="test", password="test")
        self.user.credit = 200
        self.user.save()
        response = self.buy_sim_card_credit(credit=300, token=token)
        self.assertEqual(
            response.json()[MESSAGE],
            CREDIT_AMOUNT_SHOULD_BE_POSITIVE,
        )

    def test_by_simcard_credit_authorized_user_valid_credit_success(self):
        token = self.login_and_get_token(username="test", password="test")
        self.user.credit = 200
        self.user.save()
        response = self.buy_sim_card_credit(credit=100, token=token)
        self.assertEqual(
            response.status_code,
            HTTPStatus.OK,
        )
        new_credit = Seller.objects.get(username="test").credit
        self.assertEqual(100, new_credit)

    def test_multiple_request_authorized_user_valid_credit_success(self):
        token = self.login_and_get_token(username="test", password="test")
        self.user.credit = 2000
        self.user.save()
        for _ in range(1000):
            self.buy_sim_card_credit(credit=100, token=token)

        new_credit = Seller.objects.get(username="test").credit
        self.assertEqual(0, new_credit)
        transactions_amounts = [
            t.amount
            for t in Transaction.objects.filter(seller_id=self.user.id, is_valid=True)
        ]
        sum = 0
        for amount in transactions_amounts:
            sum += amount
        self.assertEqual(2000, sum)


class TestTransactionMultiThread(TransactionTestCase):
    def setUp(self) -> None:
        self.client = Client()
        self.user = Seller(username="test")
        self.user.set_password("test")
        self.user.save()

    def login_and_get_token(self, username: str, password: str) -> str:
        client = Client()
        body = {"username": username, "password": password}
        response = client.post(
            reverse("sale:login"),
            json.dumps(body),
            content_type="application/json",
        )
        return response.json()["token"]

    def buy_sim_card_credit(self, credit, token="", pid=-1):
        body = {"credit": credit, "pid": pid}
        response = self.client.post(
            reverse("sale:buy-simcard-credit"),
            json.dumps(body),
            content_type="application/json",
            HTTP_X_ACCESS_TOKEN=token,
        )
        print(f"Process finished {pid}")
        connection.close()
        return response

    def increase_credit(self, credit, token=""):
        client = Client()
        body = {"credit": credit}
        response = client.post(
            reverse("sale:increase-credit"),
            json.dumps(body),
            content_type="application/json",
            HTTP_X_ACCESS_TOKEN=token,
        )
        connection.close()
        return response

    def test_parallel(self):
        token = self.login_and_get_token(username="test", password="test")
        credit = 0
        count = 15
        amount = 100

        self.user.credit = credit
        self.user.save()

        threads = []
        for _ in range(count):
            threads.append(
                threading.Thread(
                    target=self.increase_credit,
                    args=(amount, token),
                )
            )

        for thread in threads:
            thread.start()

        for thread in threads:
            thread.join()

        new_credit = Seller.objects.get(username="test").credit
        self.assertEqual(15, Transaction.objects.count())
        self.assertEqual(1500, new_credit)

    def test_parallel_buy(self):
        token = self.login_and_get_token(username="test", password="test")
        credit = 2000
        count = 15
        amount = 100

        self.user.credit = credit
        self.user.save()

        threads = []
        for _ in range(count):
            threads.append(
                threading.Thread(
                    target=self.buy_sim_card_credit,
                    args=(amount, token, _),
                )
            )

        for thread in threads:
            thread.start()

        for thread in threads:
            thread.join()

        new_credit = Seller.objects.get(username="test").credit
        self.assertEqual(15, Transaction.objects.count())
        self.assertEqual(500, new_credit)
