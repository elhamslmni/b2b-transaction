#!/usr/bin/env bash

python ./manage.py migrate --noinput
if [ $? -ne 0 ]; then
    echo "Migration failed :| " >&2
    exit 1
fi


# if arguments passed, execute them
# bring up an instance of project otherwise
if [[ $# -gt 0 ]]; then
    INPUT=$@
    sh -c "$INPUT"
else
    python ./manage.py runserver 0.0.0.0:8000
fi